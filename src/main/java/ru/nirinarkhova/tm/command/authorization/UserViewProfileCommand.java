package ru.nirinarkhova.tm.command.authorization;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.model.User;

import java.util.Optional;

public class UserViewProfileCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "view-profile";
    }

    @NotNull
    @Override
    public String description() {
        return "show your profile information.";
    }

    @Override
    public void execute() {
        final Optional<User> user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.get().getLogin());
        System.out.println("EMAIL: " + user.get().getEmail());
        System.out.println("FIRST NAME: " + user.get().getFirstName());
        System.out.println("LAST NAME: " + user.get().getLastName());
        System.out.println("MIDDLE NAME: " + user.get().getMiddleName());
    }

}

