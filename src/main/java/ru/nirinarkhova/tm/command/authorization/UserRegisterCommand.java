package ru.nirinarkhova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class UserRegisterCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-registration";
    }

    @NotNull
    @Override
    public String description() {
        return "register a new user in task-manager.";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRATION]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL:]");
        @NotNull final String email = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().register(login, password, email);
    }

}

