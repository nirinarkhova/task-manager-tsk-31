package ru.nirinarkhova.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.exception.system.UnknownArgumentException;

public class AboutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT:]");
        if (serviceLocator == null) throw new UnknownArgumentException();
        System.out.println(Manifests.read("developer"));
        System.out.println(Manifests.read("email"));
    }

}

