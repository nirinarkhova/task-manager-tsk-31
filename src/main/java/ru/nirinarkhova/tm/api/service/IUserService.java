package ru.nirinarkhova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.api.IService;
import ru.nirinarkhova.tm.enumerated.Role;
import ru.nirinarkhova.tm.model.User;

import java.util.List;
import java.util.Optional;

public interface IUserService extends IService<User> {

    List<User> findAll();

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    Optional<User> setPassword(@Nullable String userId,@Nullable  String password);

    @Nullable
    Optional<User> findById(@NotNull String id);

    @Nullable
    Optional<User> findByLogin(@Nullable String login);

    @Nullable
    Optional<User> findByEmail(@Nullable String email);

     boolean isEmailExist(@Nullable String email);

    @Nullable
    Optional<User> updateUser(@Nullable String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @Nullable
    User removeById(@NotNull String id);

    void removeByLogin(@Nullable String login);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}