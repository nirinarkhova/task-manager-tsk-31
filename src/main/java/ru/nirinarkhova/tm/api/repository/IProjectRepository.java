package ru.nirinarkhova.tm.api.repository;

import ru.nirinarkhova.tm.api.IBusinessRepository;
import ru.nirinarkhova.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {

}

